import { remote, BrowserWindow, Tray } from 'electron';
const crypto = require('crypto');
const qs = require("querystring");
const http = require("https");
var data;

function hash_first(string) {
	const hash = crypto.createHash('sha256');
	const value = hash.update(string).digest('hex');
	return value;
}
function hash_second(salt, string) {
	const hash = crypto.createHash('sha256');
	const value = hash.update(salt+hash_first(string)).digest('hex');
	return value;
}
function hash(_salt, passwd, _email, _ts, _secret) {
	verify(_email, btoa(hash_second(_salt, passwd)), _ts, _secret);
}

function hashSecretII(email) {
	const hash = crypto.createHash('sha256');
	const value = hash.update(email).digest('hex');
	return value;
}
function hashSecret(stamp, email) {
	const hash = crypto.createHash('sha256');
	const value = hash.update(stamp+hashSecretII(email)).digest('hex');
	return value;
}

function getSalt(_email, _ts, _secret) {
	var options = {
  		"method": "POST",
  		"hostname": "api.focuslock.dk",
  		"path": [
    		"loginHandler.php"
  		],
  		"headers": {
    		"Content-Type": "application/x-www-form-urlencoded"
  		}
	};
	var req = http.request(options, function (res) {
	  	var chunks = [];

	  	res.on("data", function (chunk) {
	    	chunks.push(chunk);
	  	});

		res.on("error", function (err) {
			console.log(err);
			res.abort();
			toggleLoad();
			message.classList.add("has-text-danger");
			message.classList.remove("has-text-succcess");
			message.innerHTML = "res error";
		});

	  	res.on("end", function () {
	    	var response = JSON.parse(Buffer.concat(chunks));
			hash(response.salt, document.getElementById("password").value, _email, _ts, _secret);
	  	});
	});
	req.write(
		qs.stringify({
			email: _email,
  			secret: _secret,
			stamp: _ts
		})
	);

	req.on("error", function (err) {
		console.log(err);
		req.abort();
		toggleLoad();
		message.classList.add("has-text-danger");
		message.classList.remove("has-text-succcess");
		message.innerHTML = "req error";
	});

	req.end();
}

function verify(_email, _passwd, _ts, _secret) {
	var message = document.getElementById("loginMessage");
	var options = {
  		"method": "POST",
  		"hostname": "api.focuslock.dk",
  		"path": [
    		"loginHandler.php"
  		],
  		"headers": {
    		"Content-Type": "application/x-www-form-urlencoded"
  		}
	};

	var req = http.request(options, function (res) {
  		var chunks = [];

  		res.on("data", function (chunk) {
    		chunks.push(chunk);
  		});

		res.on("error", function (err) {
			console.log(err);
			res.abort();
			toggleLoad();
			message.classList.add("has-text-danger");
			message.classList.remove("has-text-succcess");
			message.innerHTML = "res error";
		});

  		res.on("end", function () {
			var body = Buffer.concat(chunks);
    		var response = JSON.parse(Buffer.concat(chunks));
			if(response.response == 'granted') {
				toggleLoad();
				message.classList.add("has-text-success");
				message.classList.remove("has-text-danger");
				message.innerHTML = "Login success";
				const window = remote.getCurrentWindow();
				window.loadURL(`file://${__dirname}/index.html`);
			}
			else {
				toggleLoad();
				message.classList.add("has-text-danger");
				message.classList.remove("has-text-succcess");
				message.innerHTML = "Login failed";
			}
  		});
	});

	req.write(
		qs.stringify({
			email: _email,
			pass: _passwd,
			secret: _secret,
			stamp: _ts
		})
	);

	req.on('socket', function (socket) {
	    socket.setTimeout(5000);
	    socket.on('timeout', function() {
	        req.abort();
	    });
	});

	req.on('error', function (err) {
	    if (err.code === "ECONNRESET") {
	        console.log("Timeout occurs");
			req.abort();
			toggleLoad();
			message.classList.add("has-text-danger");
			message.classList.remove("has-text-succcess");
			message.innerHTML = "req error";
	    }
		req.abort();
		toggleLoad();
		message.classList.add("has-text-danger");
		message.classList.remove("has-text-succcess");
		message.innerHTML = "req error";
	});

	req.end();
}

function login() {
	toggleLoad();
	const _email = document.getElementById("email").value;
	const _ts = Math.round((new Date()).getTime() / 1000);
	const _secret = hashSecret(_ts, _email);

	// **************** DEBUG ONLY **************** //
	if(_email == 'admin')
	{
		const window = remote.getCurrentWindow();
		window.loadURL(`file://${__dirname}/index.html`);
	} else {
		getSalt(_email, _ts, _secret);
	}
}
function toggleLoad() {
	$('#loadModal').toggleClass('is-active');
}
