const {remote} = require('electron');

document.getElementById("close").addEventListener("click", closeWindow);
document.getElementById("minimize").addEventListener("click", minimizeWindow);
document.getElementById("questionmark").addEventListener("click", questionWindow);

function closeWindow() {
	var window = remote.getCurrentWindow();
	window.hide();
};

function minimizeWindow() {
	var window = remote.getCurrentWindow();
	window.minimize();
};

function questionWindow() {
	/** NOT IMPLEMENTED YET! **/
	alert("questionWindow(), Not implemented yet!");
};
