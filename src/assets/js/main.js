// const {Notification} = require('electron');
const {exec} = require('child_process');

$("#interface-content").load('assets/interfaces/func1.html');

/***************************************************************************************/

document.getElementById("function1").addEventListener("click", function1Btn);
document.getElementById("function2").addEventListener("click", function2Btn);
document.getElementById("function3").addEventListener("click", function3Btn);

function function1Btn() {
	$("#interface-content").load('assets/interfaces/func1.html');
};

function function2Btn() {
	$("#interface-content").load('assets/interfaces/func2.html');
};

function function3Btn() {
	$("#interface-content").load('assets/interfaces/func3.html');
};

/************ FUNC 1 EVENTS ************/

function startBtnFunc() {
	var osType = process.platform;
	console.log(osType);
	switch (osType) {
		// WINDOWS 32bit && 64bit
		case 'win32':
			try {
				console.log('win32 CASE');
				var proc = "chrome.exe";
				var cmd = 'taskkill /F /IM ' + proc;
				exec(cmd);
			} catch (e) {
				console.log(e);
			}
			break;

		// MAC OS
		case 'darwin':
			try {
				console.log('darwin CASE');
				var foo = "chrome.exe";
				exec('killall ' + foo);

			} catch (e) {
				console.log(e);
			}
			break;

		case 'linux':
			try {
				console.log('linux CASE');
				var foo = "chrome.exe";
				exec('killall ' + foo);

			} catch (e) {
				console.log(e);
			}
			break;

		case Other: //SunOS og FreeBDS
			console.log('other CASE');
			// TODO: send en besked til vores server omkring styresystem og fejl!
			try {

			} catch (e) {
				console.log(e);
			}
			break;
	}
};


/***************************************************************************************/

document.getElementById("infoBtn").addEventListener("click", infoBtn);
document.getElementById("settingsBtn").addEventListener("click", settingsBtn);

function infoBtn() {
	alert("infoBtn(), Not implemented yet!");
};

function settingsBtn() {
	document.body.classList.toggle('light-theme');
};

/***************************************************************************************/
