import { globalShortcut, app, BrowserWindow, Tray, Menu } from 'electron';

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
	app.quit();
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let loginWindow;
let loaderWindow;
let tray = null;

const createWindow = () => {
	// Create the browser window.
	mainWindow = new BrowserWindow({
		width: 900,
		minWidth: 900,
	    height: 460,
		minHeight: 460,
		frame: false,
		resizable: false,
		show: false,
		center: true,
		title: "FocusLock",
		icon: __dirname + "/assets/media/icon.png"
	});
	// Create the login window.
	loginWindow = new BrowserWindow({
		width: 900,
		minWidth: 900,
	    height: 460,
		minHeight: 460,
		frame: false,
		resizable: false,
		show: false,
		center: true,
		title: "FocusLock",
		icon: __dirname + "/assets/media/icon.png"
	});
	// Create the looader window.
	loaderWindow = new BrowserWindow({
		width: 250,
		minWidth: 250,
	    height: 300,
		minHeight: 300,
		frame: false,
		show: false,
		center: true,
		resizable: false,
		title: "FocusLock",
		icon: __dirname + "/assets/media/icon.png",
		alwaysOnTop: true
	});
	// loads the loader
	loaderWindow.loadURL(`file://${__dirname}/loader.html`);
	loaderWindow.once('ready-to-show', () => {
		loaderWindow.show();
		// and load the index.html of the app.
		loginWindow.loadURL(`file://${__dirname}/login.html`);
		mainWindow.loadURL(`file://${__dirname}/index.html`);
	})
	// Open the DevTools.
	// mainWindow.webContents.openDevTools();

	loginWindow.once('ready-to-show', () => {
		loaderWindow.destroy();
    	loginWindow.show();
		tray = new Tray(__dirname + '/assets/media/icon.png');
		const contextMenu = Menu.buildFromTemplate([
			{label: 'Open FocusLock', click: function(){ loginWindow.show(); }},
			{label: 'Close FocusLock', click: function(){ app.isQuitting = true; app.quit(); tray=null;}},
		])
		tray.setToolTip('FocusLock');
		tray.setContextMenu(contextMenu);
		tray.on('double-click', function(){ loginWindow.show(); });

		//DEV PURPPOSE ONLY!!!!!!!!!!!!!!!!!!!!!
		globalShortcut.register('f5', function() {
			console.log('f5 is pressed');
			loginWindow.reload();
		});
	});

	mainWindow.on('open', ()=> { //FIXME: Er det muligt???????
		// Sets Tray up
		tray = new Tray(__dirname + '/assets/media/icon.png');
		const contextMenu = Menu.buildFromTemplate([
			{label: 'Open FocusLock', click: function(){ mainWindow.show(); }},
			{label: 'Close FocusLock', click: function(){ app.isQuitting = true; app.quit(); tray=null;}},
		])
		tray.setToolTip('FocusLock');
		tray.setContextMenu(contextMenu);
		tray.on('double-click', function(){ mainWindow.show(); });
	});

	// Emitted when the window is closed.
	mainWindow.on('closed', () => {
	    // Dereference the window object, usually you would store windows
	    // in an array if your app supports multi windows, this is the time
	    // when you should delete the corresponding element.
	    mainWindow = null;
	});
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
	    app.quit();
	}
});

app.on('activate', () => {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
	    createWindow();
	}
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
