# Project-Stepstone

The Stepping stones for a new and better FocusLock

## How to install

First clone the repo to your desired location.

`cd` into that folder and install the dependencies with `npm install`

To start the program use the command `npm start`.<br>
If you have made changes in the `master.scss` use the command `npm run start-scss` to compile it.

I recommend to opening a second terminal, to contiunesly have the `npm run start-scss` running, so you don't have to worry about that.